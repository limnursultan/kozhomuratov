﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_домашняя_работа
{
    class Program
    {
        static void Main(string[] args)
        {
            //Создание базы данных из 30 сотрудников
            Repository repository = new Repository(30);

            //Печать всех сотрудников в консоль
            repository.Print("База данных до преоброзования");

            //Увольнение всех работников по имени "Агата"
            //repository.DeleteWorkerByName("Агата");

            //Печать в консоль всех сотрудников которые не попали под увольнение
            //repository.Print("База данных после первого преоброзования");

            //Увольнение всех работников по имени "Аделина"
            //repository.DeleteWorkerByName("Аделина");

            //Печать в консоль всех сотрудников которые не попали под увольнение
            //repository.Print("База данных после второго преоброзования");

            Console.ReadKey();
        }
    }
}
